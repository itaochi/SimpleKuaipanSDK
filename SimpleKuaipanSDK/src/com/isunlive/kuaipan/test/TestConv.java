package com.isunlive.kuaipan.test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import com.isunlive.kuaipan.KuaipanAPIFactory;
import com.isunlive.kuaipan.KuaipanAPI;
import com.isunlive.kuaipan.KuaipanAPI.ConvType;
import com.isunlive.kuaipan.KuaipanAPI.ConvView;
import com.isunlive.kuaipan.exception.KuaipanException;
import com.isunlive.kuaipan.hook.CountingOutputStream;
import com.isunlive.kuaipan.modle.KuaipanFile;
import com.isunlive.kuaipan.modle.KuaipanHTTPResponse;

public class TestConv {

	@Test
	public void testThumbnail() throws IOException, KuaipanException {
		KuaipanAPI api = KuaipanAPIFactory.getInstance();
		String path = "测试图片.gif";
		
		File f = new File("res/test.gif");
		long size = f.length();
		
		InputStream is = null;
		try {
			is = new FileInputStream(f);
		} catch (FileNotFoundException e) {
			assertTrue(false);
		}
		
		KuaipanFile file_before = api.uploadFile(path, is, size, true, null);
		try {
			is.close();
		} catch (IOException e) {}
		System.out.println(file_before);
		
		CountingOutputStream os = new CountingOutputStream();
		KuaipanHTTPResponse resp = api.thumbnail(path, os, null);
		os.close();
		KPTestUtility.openBrowser(resp.url.url);		
	}
	
	
	@Test
	public void testDocumentView() throws IOException, KuaipanException {
		KuaipanAPI api = KuaipanAPIFactory.getInstance();
		String path = "/测试文档2.doc";
		
		File f = new File("res/test.doc");
		long size = f.length();
		
		InputStream is = null;
		try {
			is = new FileInputStream(f);
		} catch (FileNotFoundException e) {
			assertTrue(false);
		}
		
		KuaipanFile file_before = api.uploadFile(path, is, size, true, null);
		try {
			is.close();
		} catch (IOException e) {}
		System.out.println(file_before);
		
		CountingOutputStream os = new CountingOutputStream();
		KuaipanHTTPResponse resp = api.documentView(ConvType.DOC, ConvView.IPAD, path, os, null);
		os.close();
		KPTestUtility.openBrowser(resp.url.url);		
	}
	
	public static void main(String []s) throws IOException, KuaipanException {
	    new TestConv().testDocumentView();
	}
}
